const Discord = require ('discord.js'); //Get with "npm i discord.js"
const fs = require("fs"); //Get with "npm i fs". Is a module (File System)

const client = new Discord.Client(); //set client at new client
const conf = JSON.parse(fs.readFileSync('./conf.json', 'utf8')); //Read conf.json File for extract the parameters
//UTF-8 is a variable width character encoding 

client.login(conf.token)


//Set all parameter with variables
var prefix = conf.prefix;
var author = conf.author;
var version = conf.version;

    client.on('ready', ()=> { //When the bot is ready to use.
        client.user.setActivity('https://gitlab.com/GillanCodes/simplediscordbot') //Set Game of the bot
        console.log('[SimpleBotDiscord] READY TO USE !'); //When bot is start a message pop in the console.
    })

    client.on('message', message => { //Event, when user send message (not on edit)

        if (message.content === prefix + "hi"){ //When user say "=hi"
            message.channel.send('Hi ' + message.author.username); //The bot send Hi "username"
        }

        if (message.content === prefix+"credit"){
            const embed = new Discord.RichEmbed()
                .setTitle("CREDIT") //Set the title of the embed
                .setAuthor('GillanCodes') //Set the author of the embed
                .setURL('https://gitlab.com/GillanCodes/simplediscordbot') //Set the URL of the embed
                .setColor('0xFFF') //Set the Color of the embed
                .setFooter('GitLab Support : https://gitlab.com/GillanCodes/simplediscordbot') //Set the footer of this embed
                .setTimestamp() //Time when the emebed was send
                
                .addField('This is a exemple of embeded message Title.', 'This is a exemple of embeded message content.') //add a field to this embed

            message.channel.send({embed}); //send the embed as message
        }

});